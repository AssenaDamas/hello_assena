import 'package:flutter/material.dart';
import 'package:hello_assena/widgets/products/product_card.dart';

class Products extends StatelessWidget {
  final List<Map<String, dynamic>> products;
  Products(this.products) {
    print('[Product Widget] Constructor');
  }

  @override
  Widget build(BuildContext context) {
    print('[Product Widget] build()');
    return products.length > 0
        ? ListView.builder(
            itemBuilder: (BuildContext context, int index) =>
                ProductCard(products[index], index),
            itemCount: products.length,
          )
        : Center(
            child: Text('No Item Found. Please Add Product First'),
          );
  }
}
