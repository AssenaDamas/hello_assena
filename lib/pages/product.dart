import 'package:flutter/material.dart';
import '../ui_elements/title_default.dart';
import '../widgets/products/location.dart';
import 'dart:async';

import '../widgets/products/price_tag.dart';

class ProductPage extends StatelessWidget {
  final String title;
  final String imageUrl;
  final double price;
  final String description;

  ProductPage({this.title, this.imageUrl, this.price, this.description});

  Widget _buildRowTitleLocationPrice() {
    return Row(
      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        TitleDefault(title),
        SizedBox(
          width: 20.0,
        ),
        Location('Demaan, Jepara'),
        Spacer(),
        PriceTag(price.toString()),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        print('Back Button Pressed');
        Navigator.pop(context, false);
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Image.asset(imageUrl),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              margin: EdgeInsets.symmetric(horizontal: 10.0),
              child: _buildRowTitleLocationPrice(),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10.0),
              child: Row(
                children: <Widget>[
                  Text('Description:'),
                  Spacer(),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Text(description),
            )
          ],
        ),
      ),
    );
  }
}
